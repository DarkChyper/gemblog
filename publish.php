<?php

namespace GemBlog;

use GemBlog\Services\Atom;
use GemBlog\Tools\DotEnv;
use GemBlog\Tools\ImageTools;
use GemBlog\Tools\PublishArticleTools;
use GemBlog\Tools\PublishPageTools;
use GemBlog\Tools\TootTools;

spl_autoload_register(static function (string $fqcn) {
    $fqcn = str_replace('GemBlog\\', '.\\src\\', $fqcn);
    $path = str_replace('\\', '/', $fqcn) . '.php';
    require_once($path);
});

function main(
    bool $newPublishedOnly,
    bool $sendToot
): void
{
    DotEnv::load('./.env.local');

    ImageTools::checkAssetsDir();

    $published = PublishArticleTools::publishArticles($newPublishedOnly);

    PublishPageTools::publishPages($published);

    Atom::generateAtomFeed($published);

    TootTools::extractDataAndToot($published, $sendToot);
}

if (!isset($argv[1]) || !isset($argv[2])) {
    echo 'This script needs 2 arguments : [bool] newPublishedOnly, [bool] sendToot, to publish posts without send to Mastodon.';
    return 1;
}

$newPublishedOnly = $argv[1] === 'true';
$sendToot = $argv[2] === 'true';
main($newPublishedOnly, $sendToot);

