<?php

namespace GemBlog\Services;

use DateTime;

class Atom
{
    public static function generateAtomFeed(
        array $articles
    ): void {

        $languages = getenv('LANGUAGES');
        $langArray = explode(',', $languages);

        foreach ($langArray as $lang) {
            $atomEntries = self::generateEntries($articles, $lang);

            $url = getenv('URL_TO_HTML');
            $feedTemplate = file_get_contents('./templates/atomFeed.xml');
            $feedTemplate = str_replace('%LANG%', $lang, $feedTemplate);
            $feedTemplate = str_replace('%GLOBAL_TITLE%', getenv('BLOG_NAME'), $feedTemplate);
            $feedTemplate = str_replace('%SUBTITLE%', getenv('BLOG_SUBTITLE_' . strtoupper($lang)), $feedTemplate);
            $feedTemplate = str_replace('%DATE%', (new DateTime())->format('Y-m-d\TH:i:s\Z'), $feedTemplate);
            $feedTemplate = str_replace('%URL%', $url, $feedTemplate);
            $feedTemplate = str_replace('%FEED_URL%', $url . 'atom_' . $lang . '.xml', $feedTemplate);
            $feedTemplate = str_replace('%CONTENT%', implode($atomEntries), $feedTemplate);
            $feedTemplate = str_replace('%AUTHOR%', getEnv('BLOG_AUTHOR'), $feedTemplate);

            $pathHtml = getenv('PATH_TO_PUBLISHED_HTML');
            file_put_contents($pathHtml . 'atom_' . $lang . '.xml', $feedTemplate);
        }
    }

    protected static function generateEntries(
        array $articles,
        string $lang = 'fr'
    ): array {
        $entryTemplate = file_get_contents('./templates/atomEntry.xml');

        $htmlLink = getenv('URL_TO_HTML');
        $atomEntries = [];

        foreach ($articles as $article) {
            if ($lang !== $article['data']->lang) {
                continue;
            }

            $template = $entryTemplate;
            $template = str_replace('%LANG%', $article['data']->lang, $template);
            $template = str_replace('%AUTHOR%', $article['data']->author, $template);
            $template = str_replace('%TITLE%', $article['data']->title, $template);
            $template = str_replace('%URL_BLOG%', $htmlLink, $template);
            $template = str_replace('%URL_ARTICLE%', $htmlLink.$article['data']->fileName.'.html', $template);
            $template = str_replace('%PUBLISHED_DATE%', $article['data']->publishedAt, $template);
            $template = str_replace('%UPDATED_DATE%', $article['data']->updatedAt, $template);
            $template = str_replace('%CONTENT%', htmlspecialchars($article['content']), $template);
            $template = str_replace('%SUMMARY%', $article['data']->description, $template);

            $atomEntries[] = $template;
        }

        return $atomEntries;
    }
}