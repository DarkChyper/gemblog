<?php

namespace GemBlog\Services;


class Mastodon
{
    public static function postToot(
        $instanceUrl,
        $accessToken,
        $lang,
        $messageHtml,
        $messageGmi
    ): string|false
    {
        $headers = array(
            'Authorization: Bearer ' . $accessToken,
            "Content-Type: application/json"
        );

        // Set the request URL
        $url = $instanceUrl . 'api/v1/statuses';

        // Set the request parameters
        $params = array(
            'status' => $messageHtml,
            'language' => $lang,
            'visibility' => 'public'
        );

        $jsonResponse = self::toot($url, $headers, $params);

        if (!$jsonResponse)
            return false;

        $messageUrl = $jsonResponse->url;
        $messageId = $jsonResponse->id;
        $username = $jsonResponse->account->username;

        $params = array(
            "status" => '@' . $username . ' ' . $messageGmi,
            "in_reply_to_id" => $messageId
        );
        self::toot($url, $headers, $params);

        return $messageUrl;
    }

    protected static function toot(string $url, array $headers, array $params): mixed
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $jsonResponse = json_decode($response);

        curl_close($ch);

        return $jsonResponse;
    }
}