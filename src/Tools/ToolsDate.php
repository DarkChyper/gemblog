<?php

namespace GemBlog\Tools;

class ToolsDate
{
    public static function getNow(bool $resetTime = true): \DateTime
    {
        $now = new \DateTime();

        if ($resetTime) {
            $now->setTime(0, 0);
        }

        return $now;
    }
}