<?php

namespace GemBlog\Tools;

use Exception;

class GeminiTools
{
    public const GEMINI_SEPARATOR = '---';
    public const GEMINI_H1 = '# ';
    public const GEMINI_H2 = '## ';
    public const GEMINI_H3 = '### ';
    public const GEMINI_LIST = '*';
    public const GEMINI_QUOTE = '>';
    public const GEMINI_LINK = '=>';
    public const GEMINI_FREE = '```';
    public const GEMINI_X = '!#';
    public const GEMINI_CODE = 'code';
    public const GEMINI_SPOILER = 'spoiler';

    protected static int $codeId = -1;

    public static function extractDescriptionFromFile(string $pathFile): string
    {
        $openFile = fopen($pathFile, "r");
        $returnedLine = '';
        $line = fgets($openFile);
        $prevLineType = '';

        while ($line) {
            if (self::isGeminiFree($line) || self::GEMINI_FREE === $prevLineType) {
                $prevLineType = self::isGeminiFreeEnd($line) ? '' : self::GEMINI_FREE;
                $line = fgets($openFile);
                continue;
            }

            if (self::isSimpleLine($line)) {
                $words = explode(' ', $line, 15);
                $returnedLine = ucFirst(implode(' ', $words) . '...');
                $line = false;
                continue;
            }
            $line = fgets($openFile);
        }

        fclose($openFile);

        return $returnedLine;
    }

    /**
     * @param array<string,string|array<int,string>|array<string,string|int> $articleMetadata
     * @throws Exception
     */
    public static function publishCapsule(string $filePath, mixed $articleMetadata): array
    {
        $targetPath = (string)getenv('PATH_TO_PUBLISHED_GMI');
        $htmlLink = (string)getenv('URL_TO_HTML');
        $gemLink = (string)getenv('URL_TO_GMI');
        $articleTemplate = file_get_contents('./templates/article.gmi');

        $fileContent = file($filePath);
        if (!$fileContent)
            throw new Exception('File ' . $filePath . ' doesn\'t exists');

        if ($articleMetadata->type === 'gmix') {
            $image = trim(substr($fileContent[3], 2));
            if (!empty($image)) {
                $imageSplit = explode(' ', $image, 2);
                ImageTools::fileImageToProdImage($imageSplit[0]);
            }
            unset($fileContent[0], $fileContent[1], $fileContent[2], $fileContent[3], $fileContent[4]);
        }
        $fileContentToHtml = $fileContent;
        self::dealWithPictures($fileContent);
        self::extractHiddenCodeToFiles($fileContent, $articleMetadata, $gemLink, $targetPath);

        $image = isset($articleMetadata->banner->filePath) ?
            '=> ./assets/' . $articleMetadata->banner->filePath . ' ' . $articleMetadata->banner->fileDescription
            : '';

        $tags = '';
        foreach ($articleMetadata->tags as $tag) {
            $tags .= '=> ./articles_' . $tag . '.gmi #' . $tag . chr(10);
        }

        $articleTemplate = str_replace('%TITLE%', $articleMetadata->title, $articleTemplate);
        $articleTemplate = str_replace('%IMG%', $image, $articleTemplate);
        $articleTemplate = str_replace('%DATE%', $articleMetadata->publishedAt, $articleTemplate);
        $articleTemplate = str_replace('%DATE_SHORT%', $articleMetadata->publishedAt, $articleTemplate);
        $articleTemplate = str_replace('%CONTENT%', implode('', $fileContent), $articleTemplate);
        $articleTemplate = str_replace('%HTML_LINK%', $htmlLink . $articleMetadata->fileName . '.html', $articleTemplate);
        $articleTemplate = str_replace('%HTML_WEBSITE%', $htmlLink, $articleTemplate);
        $articleTemplate = str_replace('%GEM_LINK%', $gemLink . $articleMetadata->fileName . '.gmi', $articleTemplate);
        $articleTemplate = str_replace('%TAGS%', $tags, $articleTemplate);

        file_put_contents($targetPath . $articleMetadata->fileName . '.gmi', $articleTemplate);

        return $fileContentToHtml;
    }

    public static function gmiToHtml(
        string &$htmlContent,
        string $line,
        string $prevLineType
    ): string
    {
        return match (true) {
            self::isGeminiX($line) && self::isNotFree($prevLineType) => '',
            self::isGeminiEmpty($line) && self::isNotFree($prevLineType) => self::gmiEmptyToHtml($htmlContent, $prevLineType),
            self::isGeminiSeparator($line) && self::isNotFree($prevLineType) => self::gmiSeparatorToHtml($htmlContent, $prevLineType),
            self::isGeminiH1($line) && self::isNotFree($prevLineType) => self::gmiHXToHtml($htmlContent, $line, $prevLineType, 1),
            self::isGeminiH2($line) && self::isNotFree($prevLineType) => self::gmiHXToHtml($htmlContent, $line, $prevLineType, 2),
            self::isGeminiH3($line) && self::isNotFree($prevLineType) => self::gmiHXToHtml($htmlContent, $line, $prevLineType, 3),
            self::isGeminiList($line) && self::isNotFree($prevLineType) => self::gmiListToHtml($htmlContent, $line, $prevLineType),
            self::isGeminiQuote($line) && self::isNotFree($prevLineType) => self::gmiQuoteToHtml($htmlContent, $line, $prevLineType),
            self::isGeminiLink($line) && self::isNotFree($prevLineType) => self::gmiLinkToHtml($htmlContent, $line, $prevLineType),
            self::isGeminiFree($line),
            self::isGeminiFree($prevLineType),
            self::isGeminiSpoiler($prevLineType),
            self::isGeminiCode($prevLineType) => self::gmiFreeToHtml($htmlContent, $line, $prevLineType),
            default => self::gmiContentToHtml($htmlContent, $line, $prevLineType)
        };

    }

    protected static function extractHiddenCodeToFiles(
        array  &$fileContent,
        object $publishedData,
        string $gemLink,
        string $targetPath
    ): void
    {
        $regex = (string)getenv('CODE_LANGUAGE');
        $linesToUnset = [];
        $pageName = '';
        $linesToNewPage = [];
        $prevLine = '';
        $numberOfSubFiles = 0;
        foreach ($fileContent as $key => $line) {
            if (self::isGeminiFree($line) && !self::isGeminiFree($prevLine)) {

                $line = trim(substr($line, 3));

                if (preg_match($regex, $line)) {
                    $parts = explode(' ', $line);

                    $isSpoiler = 'SPOILER' === trim($parts[0]) || (isset($parts[1]) && 'SPOILER' === trim($parts[1]));

                    if (!$isSpoiler)
                        continue;

                    $pageName = $parts[0] . '.' . $numberOfSubFiles++ . '.gmi';
                    $linesToUnset[] = $key;
                    $prevLine = self::GEMINI_FREE;
                }

            } elseif (self::isGeminiFree($prevLine)) {
                if (self::isGeminiFreeEnd($line)) {
                    $linesToNewPage[] = substr($line, 0, strlen($line) - 3);

                    $codeTemplate = file_get_contents('./templates/code.gmi');
                    $codeTemplate = str_replace('%LINK_TO_ARTICLE%', $gemLink . $publishedData->fileName . '.gmi', $codeTemplate);
                    $codeTemplate = str_replace('%CONTENT%', implode('', $linesToNewPage), $codeTemplate);

                    if (!is_dir($targetPath . $publishedData->fileName . '/')) {
                        mkdir($targetPath . $publishedData->fileName . '/');
                    }
                    file_put_contents($targetPath . $publishedData->fileName . '/' . $pageName, $codeTemplate);

                    $fileContent[$key] = '=> ' . $targetPath . $publishedData->fileName . '/' . $pageName . ' ' . $pageName;

                    $pageName = [];
                    $linesToNewPage = [];
                    $prevLine = '';
                } else {
                    $linesToUnset[] = $key;
                    $linesToNewPage[] = $line;
                }
            }
        }


        foreach ($linesToUnset as $lineToUnset) {
            unset($fileContent[$lineToUnset]);
        }
    }

    protected static function dealWithPictures(
        array &$fileContent
    ): void
    {
        foreach ($fileContent as $key => $line) {
            if (self::isGeminiLink($line)) {
                $parts = explode(' ', $line, 3);
                if (str_starts_with(trim($parts[1]), 'files://') && preg_match('/.*png|jpg|jpeg|bmp|gif$/', trim($parts[1]))) {
                    $imageFullName = substr($parts[1], 8);
                    $link = ImageTools::fileImageToProdImage($imageFullName, true);
                    $fileContent[$key] = '=> ' . $link . ' ' . $parts[2];
                }
            }
        }
    }

    protected static function gmiEmptyToHtml(string &$htmlContent, string $prevLine): string
    {
        self::verifyPrevLine($htmlContent, $prevLine);

        $htmlContent .= HtmlTools::BR;
        return '';
    }

    protected static function gmiSeparatorToHtml(string &$htmlContent, string $prevLine): string
    {
        self::verifyPrevLine($htmlContent, $prevLine);
        $htmlContent .= HtmlTools::HR;
        return '';
    }

    protected static function gmiListToHtml(string &$htmlContent, string $line, string $prevLine): string
    {
        if (!self::isGeminiList($prevLine)) {
            $htmlContent .= HtmlTools::UL_START;
        }
        if (self::isGeminiQuote($prevLine)) {
            $htmlContent .= HtmlTools::BQ_END;
        }
        $htmlContent .= HtmlTools::LI_START . substr($line, 2) . HtmlTools::LI_END;
        return self::GEMINI_LIST;
    }

    protected static function gmiHXToHtml(string &$htmlContent, string $line, string $prevLine, int $level): string
    {
        self::verifyPrevLine($htmlContent, $prevLine);

        $htmlContent .= match ($level) {
            1 => HtmlTools::H1_START . substr($line, 1) . HtmlTools::H1_END,
            2 => HtmlTools::H2_START . substr($line, 2) . HtmlTools::H2_END,
            default => HtmlTools::H3_START . substr($line, 3) . HtmlTools::H3_END
        };

        return '';
    }

    protected static function gmiQuoteToHtml(string &$htmlContent, string $line, string $prevLine): string
    {
        if (self::isGeminiList($prevLine)) {
            $htmlContent .= HtmlTools::UL_END;
        }

        if (!self::isGeminiQuote($prevLine)) {
            $htmlContent .= HtmlTools::BQ_START;
        }

        $htmlContent .= substr($line, 1) . HtmlTools::BR;
        return self::GEMINI_QUOTE;
    }

    protected static function gmiLinkToHtml(string &$htmlContent, string $line, string $prevLine): string
    {
        self::verifyPrevLine($htmlContent, $prevLine);

        $exploded = explode(' ', substr($line, 3), 2);
        $link = $exploded[0];
        $comment = $exploded[1] ?? $exploded[0];

        $htmlContent .= HtmlTools::P_START;
        if (preg_match('/^(http:\/\/|https:\/\/|gemini:\/\/)/', $link)) {

            $htmlContent .= sprintf(HtmlTools::A_START_REL_CLASS, $link, 'external', 'external') . $comment . HtmlTools::A_END;
        } elseif (preg_match('/\.(jpg|jpeg|png|bmp|gif)$/i', $link)) {

            if (str_starts_with($link, 'files://')) {
                $imageFullName = substr($link, 8);
                $link = ImageTools::fileImageToProdImage($imageFullName);
            }

            $htmlContent .= sprintf(HtmlTools::A_START, $link);
            $htmlContent .= sprintf(HtmlTools::IMG, $link, $comment, 'center rounded');
            $htmlContent .= HtmlTools::A_END;
        } elseif (preg_match('/\.(gmi)$/i', $link)) {

            $link = preg_replace('/(\.gmi)$/', '.html', $link);
            $htmlContent .= sprintf(HtmlTools::A_START, $link) . $comment . HtmlTools::A_END;
        } else {

            $htmlContent .= sprintf(HtmlTools::DIV_START, 'alert alert-error') . 'error with : ' . $link . ' => ' . $comment. HtmlTools::DIV_END;
        }

        $htmlContent .= HtmlTools::P_END;
        return self::GEMINI_LINK;
    }

    protected static function gmiFreeToHtml(string &$htmlContent, string $line, string $prevLine): string
    {
        self::verifyPrevLine($htmlContent, $prevLine);

        if (!self::isGeminiFree($prevLine) && !self::isGeminiCode($prevLine) && !self::isGeminiSpoiler($prevLine)) {
            $regex = (string)getenv('CODE_LANGUAGE');
            $id = 'code-'.++self::$codeId;
            $line = trim(substr($line, 3));

            if (preg_match($regex, $line)) {
                $parts = explode(' ', $line);
                $isSpoiler = self::GEMINI_SPOILER === strtolower(trim($parts[0])) || (isset($parts[1]) && self::GEMINI_SPOILER === strtolower(trim($parts[1])));
                $prevLine = self::GEMINI_CODE;
                $altCode = 'language '.$parts[0];
                $classLang = 'lang';
                $classCode = 'overflow-auto';

                $htmlContent .= sprintf(HtmlTools::PRE_START, '');;
                $textSpoiler = '&nbsp;';
                if ($isSpoiler)
                {
                    $textSpoiler = '&#x25BD; SPOILER';
                    $classCode .= ' hide-spoiler';
                    $prevLine = self::GEMINI_SPOILER;
                }
                $htmlContent .= sprintf(HtmlTools::DIV_START_ONCLICK, 'spoiler-container', $id, $textSpoiler);
                $htmlContent .= sprintf(HtmlTools::SPAN_START, $classLang);
                $htmlContent .= $parts[0];
                $htmlContent .= HtmlTools::SPAN_END;
                $htmlContent .= HtmlTools::DIV_END;
                $htmlContent .= HtmlTools::HR;
                $htmlContent .= sprintf(HtmlTools::CODE_START, $id , $classCode, $altCode, strtolower($parts[0]));

                // on ne copie pas la ligne qui indique le langage
                return $prevLine;
            } else {
                $htmlContent .= sprintf(HtmlTools::PRE_START, 'overflow-auto');
            }
        }

        if (self::isGeminiFreeEnd($line)) {
            $htmlContent .= substr($line, 0, strlen($line) - 4);

            if (self::isGeminiSpoiler($prevLine) || self::isGeminiCode($prevLine)) {
                $htmlContent .= HtmlTools::CODE_END;
            }
            $htmlContent .= HtmlTools::PRE_END;


            $prevLine = '';
        } else {
            $htmlContent .= HtmlTools::sanitize($line);
            $prevLine = self::GEMINI_CODE === $prevLine ? self::GEMINI_CODE : (self::GEMINI_SPOILER === $prevLine? self::GEMINI_SPOILER : self::GEMINI_FREE);
        }

        return $prevLine;
    }

    protected static function gmiContentToHtml(string &$htmlContent, string $line, string $prevLine): string
    {
        self::verifyPrevLine($htmlContent, $prevLine);

        $htmlContent .= HtmlTools::P_START . $line . HtmlTools::P_END;
        return '';
    }

    protected static function isSimpleLine(string $line): bool
    {
        if (self::isGeminiFree($line)
            || self::isGeminiSeparator($line)
            || self::isGeminiH1($line)
            || self::isGeminiH2($line)
            || self::isGeminiH3($line)
            || self::isGeminiQuote($line)
            || self::isGeminiList($line)
            || self::isGeminiLink($line)
            || self::isGeminiFree($line))
            return false;

        return true;
    }

    protected static function verifyPrevLine(string &$htmlContent, string $prevLine): void
    {
        if (self::isGeminiList($prevLine)) {
            $htmlContent .= HtmlTools::UL_END;
        }
        if (self::isGeminiQuote($prevLine)) {
            $htmlContent .= HtmlTools::BQ_END;
        }
    }

    private static function isGeminiEmpty(string $line): bool
    {
        return empty(trim($line));
    }

    private static function isGeminiX(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_X);
    }

    private static function isGeminiSeparator(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_SEPARATOR);
    }

    private static function isGeminiH1(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_H1);
    }

    private static function isGeminiH2(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_H2);
    }

    private static function isGeminiH3(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_H3);
    }

    private static function isGeminiList(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_LIST);
    }

    private static function isGeminiQuote(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_QUOTE);
    }

    private static function isGeminiLink(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_LINK);
    }

    private static function isGeminiFree(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_FREE) || str_ends_with(trim($line), self::GEMINI_FREE);
    }

    private static function isGeminiCode(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_CODE);
    }

    private static function isGeminiSpoiler(string $line): bool
    {
        return str_starts_with($line, self::GEMINI_SPOILER);
    }

    private static function isGeminiFreeEnd(string $line): bool
    {
        return str_ends_with(trim($line), self::GEMINI_FREE);
    }

    private static function isNotFree(string $prevLineType) : bool
    {
        return !self::isGeminiFree($prevLineType) && !self::isGeminiCode($prevLineType) && !self::isGeminiSpoiler($prevLineType);
    }
}

