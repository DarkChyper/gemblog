<?php

namespace GemBlog\Tools;


class PublishTools
{
    public static function addMastodonToGmi(false|string $url, string $article): void
    {
        self::addMastodonTo($url, $article, './templates/mastodon.gmi');
    }

    public static function addMastodonToHtml(false|string $url, string $article): void
    {
        self::addMastodonTo($url, $article, './templates/mastodon.html');
    }

    protected static function addMastodonTo(false|string $url, string $article, string $template): void
    {
        $articleToFill = file_get_contents($article);
        if (!$url)
        {
            $articleToFill = str_replace('%MASTODON%', '', $articleToFill);
        }
        else
        {
            $templateToFill = file_get_contents($template);
            $templateToFill = str_replace('%MASTODON_URL%', $url, $templateToFill);
            $articleToFill = file_get_contents($article);
            $articleToFill = str_replace('%MASTODON%', $templateToFill, $articleToFill);
        }

        file_put_contents($article, $articleToFill);

    }
}
