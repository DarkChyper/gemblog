<?php

namespace GemBlog\Tools;


class ImageTools
{
    protected static array $cacheImage = [];
    protected static array $cacheDirectory = [];

    public static function checkAssetsDir(): void {
        $pathToPublishedGmi = getenv('PATH_TO_PUBLISHED_GMI') . 'assets/';
        if (!file_exists($pathToPublishedGmi))
        {
            mkdir($pathToPublishedGmi);
            if (!file_exists($pathToPublishedGmi)) {
                echo $pathToPublishedGmi . ' ne peut pas être créé ! ';die();
            }
        }

        $pathToPublishedHtml = getenv('PATH_TO_PUBLISHED_HTML') . 'assets/';
        if (!file_exists($pathToPublishedHtml))
        {
            mkdir($pathToPublishedHtml);
            if (!file_exists($pathToPublishedHtml)) {
                echo $pathToPublishedHtml . ' ne peut pas être créé ! ';die();
            }
        }
    }

    public static function fileImageToProdImage(string $imageFullName, bool $isGemini = false): string
    {
        if (isset(self::$cacheImage[$imageFullName]))
            return $isGemini ? self::$cacheImage[$imageFullName]['gemini']: self::$cacheImage[$imageFullName]['html'];

        $pathToSource = getenv('PATH_TO_SOURCE') . 'assets/';

        if (!file_exists($pathToSource.$imageFullName)) {
            echo 'Le fichier ' . $pathToSource.$imageFullName .' est introuvable.';
            return '';
        }

        $pathToPublishedGmi = getenv('PATH_TO_PUBLISHED_GMI') . 'assets/';
        $pathToPublishedHtml = getenv('PATH_TO_PUBLISHED_HTML') . 'assets/';
        $urlToGmi = getenv('URL_TO_GMI') . 'assets/';
        $urlToHtml = getenv('URL_TO_HTML') . 'assets/';

        // creation des dossiers si besoin
        $directories = explode('/', $imageFullName);

        $size = sizeof($directories);
        $directory = '';
        for ($d=0; $d < $size - 1; $d++) {
            $directory = $directories[$d].'/';
            if (!isset(self::$cacheDirectory[$directory]))
            {
                $creationGmi = false;
                $creationHtml = false;
                if (!file_exists($pathToPublishedGmi.$directory))
                    $creationGmi = mkdir($pathToPublishedGmi.$directory);
                if (!file_exists($pathToPublishedHtml.$directory))
                    $creationHtml = mkdir($pathToPublishedHtml.$directory);
                if ($creationGmi || $creationHtml)
                    self::$cacheDirectory[$directory] = ['gemini' => $pathToPublishedGmi.$directory, 'html' => $pathToPublishedHtml.$directory];
            }
        }

        // redimensionnement et optimisation de l'image
        $finalPathToImage = $directory.$directories[$size - 1];
        self::resizeImage($pathToSource.$finalPathToImage);

        // optimisation de l'image
        // TODO

        // copie du fichier dans les dossiers de prod
        copy($pathToSource.$finalPathToImage, $pathToPublishedGmi.$finalPathToImage);
        copy($pathToSource.$finalPathToImage, $pathToPublishedHtml.$finalPathToImage);

        // mise en cache des url de l'image
        self::$cacheImage[$imageFullName] = ['gemini' => $urlToGmi.$finalPathToImage, 'html' => $urlToHtml.$finalPathToImage];

        return $isGemini ? self::$cacheImage[$imageFullName]['gemini']: self::$cacheImage[$imageFullName]['html'];
    }
    protected static function resizeImage(string $file): void {
        if (preg_match('/\.(jpg|jpeg|png|bmp|gif)$/i', $file)) {
            // Load the image from file
            $image = imagecreatefromstring(file_get_contents($file));

            $maxWidth = getenv('IMAGE_MAX_WIDTH');
            // Get the current dimensions of the image
            $width = imagesx($image);
            $height = imagesy($image);

            // Calculate the new height based on the maximum width
            $newHeight = floor($height * ($maxWidth / $width));

            // Create a new empty image with the new dimensions
            $newImage = imagecreatetruecolor($maxWidth, $newHeight);

            // Resize the original image to the new dimensions
            imagecopyresampled($newImage, $image, 0, 0, 0, 0, $maxWidth, $newHeight, $width, $height);

            // Output the resized image to file
            match (strtolower(pathinfo($file, PATHINFO_EXTENSION)))
            {
                'jpg', 'jpeg' => imagejpeg($newImage, $file),
                'png' => imagepng($newImage, $file),
                'bmp' =>  imagewbmp($newImage, $file),
                'gif' => imagegif($newImage, $file)
            };

            // Free up memory by destroying the images
            imagedestroy($image);
            imagedestroy($newImage);
        }
    }
}

