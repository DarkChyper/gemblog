<?php

namespace GemBlog\Tools;

class PublishArticleTools
{
    private const REGEX_ENABLED_TO_PUBLISHED_FILE = '/^(\d{4}-\d{2}-\d{2})-([a-z0-9-])+\.gmix$/i';

    public static function publishArticles(
        bool $newPublishedOnly
    ): array
    {
        $published = [];

        $now = ToolsDate::getNow(false);
        $languages = explode(',', getenv('LANGUAGES'));

        foreach ($languages as $language) {
            $published = self::publishByLanguage($published, $now, $language, $newPublishedOnly);
        }

        return $published;
    }

    protected static function publishByLanguage(array $published, \DateTime $now, string $language, bool $newPublishedOnly): array
    {
        $sourcePath = getenv('PATH_TO_SOURCE') . $language . '/';
        $fileNames = scandir($sourcePath);

        foreach ($fileNames as $fileName) {
            if (self::isFileNotEnabledToProcess($fileName))
                continue;

            if (self::isArticlePlannedForFuture($fileName, $now))
                continue;

            $actualCheckSum = md5_file($sourcePath . $fileName);

            $articleMetadataFileName = self::getOrCreateMetadataOfArticle($sourcePath, $fileName, $language, $now);
            $articleMetadata = json_decode(file_get_contents($sourcePath.$articleMetadataFileName));
            if (!isset($articleMetadata->type)) {
                $isGeminiX = str_ends_with($fileName, 'x');
                $articleMetadata->type = $isGeminiX ? 'gmix' : 'gmi';
                file_put_contents($sourcePath.$articleMetadataFileName, json_encode($articleMetadata, JSON_PRETTY_PRINT));
            }

            $doMaj = $articleMetadata->hash !== $actualCheckSum || !$newPublishedOnly;
            try {
                $fileContent = GeminiTools::publishCapsule($sourcePath . $fileName, $articleMetadata);
                $htmlContent = HtmlTools::extractHtmlContent($fileContent);
                if ($doMaj)
                    HtmlTools::publishArticle($htmlContent, $articleMetadata);

                if ($articleMetadata->hash !== $actualCheckSum)
                    $articleMetadata->updatedAt = $now->format('Y-m-d\TH:i:s\Z');

                $articleMetadata->hash = $actualCheckSum;
                file_put_contents($sourcePath.$articleMetadataFileName, json_encode($articleMetadata, JSON_PRETTY_PRINT));

                $published[$articleMetadata->fileName . '__' . $articleMetadata->lang] = ['data' => $articleMetadata, 'content' => $htmlContent, 'metadataFileName' => $articleMetadataFileName];
            } catch (\Exception $e) {
                // TODO log
                echo $e->getFile() . ' ' . $e->getLine() . ' : ' . $e->getMessage();
                continue;
            }
        }

        uksort($published, function ($a, $b) {
            return strnatcmp($b, $a);
        });

        return $published;
    }

    protected static function isFileNotEnabledToProcess(string $fileName): bool
    {
        return ('.' === $fileName || '..' === $fileName || !preg_match(self::REGEX_ENABLED_TO_PUBLISHED_FILE, $fileName));
    }

    protected static function isArticlePlannedForFuture(string $fileName, \DateTime $now): bool
    {
        if (preg_match(self::REGEX_ENABLED_TO_PUBLISHED_FILE, $fileName, $matches)) {
            $fileDate = \DateTime::createFromFormat('Y-m-d', $matches[1]);

            if ($fileDate instanceof \DateTime && $fileDate > $now)
                return true;
        }

        return false;
    }

    protected static function getOrCreateMetadataOfArticle(string $path, string $fileName, string $language, \DateTime $now): string
    {
        $isGeminiX = str_ends_with($fileName, 'x');
        $length = $isGeminiX ? 5 : 4;

        $articleMetadataFileName = substr($fileName, 0, strlen($fileName) - $length);

        if (!file_exists($path.$articleMetadataFileName)) {
            if ($isGeminiX)
                [$title, $description, $tags, $banner, $author] = self::extractMetadataFromGMIX($path, $fileName);
            else
                [$title, $description, $tags, $banner, $author] = self::extractMetadataFromGMI($path, $fileName);

            $fileNameLang = $language !== getenv('MAIN_LANGUAGE') ? '-' . $language : '';
            $releasedData = array(
                "hash" => "",
                "type" => $isGeminiX ? 'gmix' : 'gmi',
                "lang" => $language,
                "title" => $title,
                "author" => $author,
                "description" => $description,
                "tags" => $tags,
                "banner" => array("filePath" => $banner[0] ?? "", "fileDescription" => $banner[1] ?? ""),
                "fileName" => $articleMetadataFileName . $fileNameLang,
                "publishedAt" => $now->format('Y-m-d\TH:i:s\Z'),
                "updatedAt" => "",
                "urlToot" => ""
            );

            file_put_contents($path.$articleMetadataFileName, json_encode($releasedData, JSON_PRETTY_PRINT));
        }

        return $articleMetadataFileName;
    }

    /**
     * @return array<int,string|array<int,string>>
     */
    protected static function extractMetadataFromGMIX(string $path, string $fileName): array
    {
        $openFile = fopen($path . $fileName, "r");

        $titleLine = fgets($openFile);
        $title = HtmlTools::sanitize(substr($titleLine, 3));

        $descriptionLine = fgets($openFile);
        $description = HtmlTools::sanitize(substr($descriptionLine, 3));

        $tagsLine = fgets($openFile);
        $tags = explode(",", substr($tagsLine, 3));
        array_walk($tags, function (&$value) {
            $value = HtmlTools::sanitize(trim($value));
        });

        $bannerLine = fgets($openFile);
        $banner = explode(" ", substr($bannerLine, 3), 2);
        array_walk($banner, function (&$value) {
            $value = trim($value);
        });

        $authorLine = fgets($openFile);
        $author = !str_starts_with($authorLine, '!#') || '' !== trim(substr($descriptionLine, 2)) ?
            getenv('BLOG_AUTHOR') : trim(substr($authorLine, 2));

        fclose($openFile);

        return [$title, $description, $tags, $banner, $author];
    }

    /**
     * @return array<int,string|array<int,string>>
     */
    protected static function extractMetadataFromGMI(string $path, string $fileName): array
    {
        $title = '';
        if (preg_match(self::REGEX_ENABLED_TO_PUBLISHED_FILE, $fileName, $matches)) {
            $words = explode('-', $matches[2]);
            $title = implode(' ', $words);
            $title = ucfirst($title);
        }
        $description = GeminiTools::extractDescriptionFromFile($path . $fileName);
        return [$title, $description, [], '', ''];
    }
}
