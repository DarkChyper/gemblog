<?php

namespace GemBlog\Tools;

use GemBlog\Services\Mastodon;

class TootTools
{
    public static function extractDataAndToot(array $published, bool $sendToot): void
    {
        foreach ($published as $article) {
            if ($sendToot && empty($article['data']->urlToot)) {
                $messageGmi = '[gemini] ' . $article['data']->description . ' ' . getenv('URL_TO_GMI') . $article['data']->fileName . '.gmi ';
                $messageHtml = $article['data']->description;
                $messageHtml .= ' ' . getenv('URL_TO_HTML') . $article['data']->fileName . '.html ';
                foreach ($article['data']->tags as $tag) {
                    $messageHtml .= '#' . $tag . ' ';
                }

                $tootUrl = Mastodon::postToot(getenv('MASTODON_URL'), getenv('MASTODON_ACCESS_TOKEN'), $article['data']->lang, $messageHtml, $messageGmi);

                $article['data']->urlToot = $tootUrl;
                file_put_contents($article['metadataFileName'], json_encode($article['data'], JSON_PRETTY_PRINT));
            }

            PublishTools::addMastodonToGmi($article['data']->urlToot, getenv('PATH_TO_PUBLISHED_GMI') . $article['data']->fileName . '.gmi');
            PublishTools::addMastodonToHtml($article['data']->urlToot, getenv('PATH_TO_PUBLISHED_HTML') . $article['data']->fileName . '.html');

        }
    }

}
