<?php

namespace GemBlog\Tools;

use Normalizer;

class HtmlTools
{
    public const A_START = '<a href="%s">';
    public const A_START_REL_CLASS = '<a href="%s" rel="%s" class="%s">';
    public const A_END = '</a>';
    public const BQ_START = '<blockquote>';
    public const BQ_END = '</blockquote>';
    public const BR = '<br />';
    public const CODE_START = '<code id="%s" class="%s" alt="%s" lang="%s">';
    public const CODE_END = '</code>';
    public const DIV_START = '<div class="%s">';
    public const DIV_START_ONCLICK = '<div class="%s code" onclick="switchSpoiler(\'%s\');">%s';
    public const DIV_END = '</div>';
    public const H1_START = '<h1>';
    public const H1_END = '</h1>';
    public const H2_START = '<h2>';
    public const H2_END = '</h2>';
    public const H3_START = '<h3>';
    public const H3_END = '</h3>';
    public const HR = '<hr>';
    public const IMG = '<img src="%s" alt="%s" class="%s" />';
    public const LI_START = '<li>';
    public const LI_END = '</li>';
    public const P_START = '<p>';
    public const P_END = '</p>';
    public const PRE_START = '<pre class="%s">';
    public const PRE_END = '</pre>';
    public const SPAN_START = '<span class="%s">';
    public const SPAN_END = '</span>';
    public const UL_START = '<ul>';
    public const UL_END = '</ul>';

    public static function extractHtmlContent(array $fileContent): string
    {
        $htmlContent = '';
        $prevLine = '';
        foreach ($fileContent as $line) {
            $prevLine = GeminiTools::gmiToHtml($htmlContent, $line, $prevLine);
        }

        return $htmlContent;
    }

    public static function publishArticle(string $htmlContent, mixed $articleMetadata): void
    {

        $htmlLink = getenv('URL_TO_HTML');
        $gemLink = getenv('URL_TO_GMI');
        $targetPath = getenv('PATH_TO_PUBLISHED_HTML');
        $author = $articleMetadata->author;
        $articleTemplate = file_get_contents('./templates/article.html');

        $subtitle = match ($articleMetadata->lang) {
            'fr' => 'Publié le ' . substr($articleMetadata->publishedAt, 0, 10) . ' par ' . $author,
            'en' => 'Published at ' . substr($articleMetadata->publishedAt, 0, 10) . ' by ' . $author
        };

        $imageHtml = str_ends_with($articleMetadata->banner->filePath, getenv('BLOG_BANNER')) ?
            '' :
            '<img alt="'
            . $articleMetadata->banner->fileDescription
            . '" src="'
            . $htmlLink.'/assets/' . $articleMetadata->banner->filePath
            . '" width="600px" class="center rounded" />';

        $tags = '';
        foreach ($articleMetadata->tags as $tag) {
            $tags .= '<a href="./articles_' . $tag . '.html">#' . $tag . '</a> ';
        }

        $imagePreview = $htmlLink . 'assets/';
        $imagePreview .= empty($articleMetadata->banner->filePath) ? getenv('BLOG_BANNER') : $articleMetadata->banner->filePath;

        $articleTemplate = str_replace('%LANG%', $articleMetadata->lang, $articleTemplate);
        $articleTemplate = str_replace('%TITLE%', $articleMetadata->title, $articleTemplate);
        $articleTemplate = str_replace('%HTML_LINK%', $htmlLink . $articleMetadata->fileName . '.html', $articleTemplate);
        $articleTemplate = str_replace('%GEM_LINK%', $gemLink . $articleMetadata->fileName . '.gmi', $articleTemplate);
        $articleTemplate = str_replace('%PUBLISHED_DATE%', $articleMetadata->publishedAt, $articleTemplate);
        $articleTemplate = str_replace('%IMAGE_PREVIEW%', $imagePreview, $articleTemplate);
        $articleTemplate = str_replace('%IMAGE_HEADER%', $imageHtml, $articleTemplate);
        $articleTemplate = str_replace('%SUBTITLE%', $subtitle, $articleTemplate);
        $articleTemplate = str_replace('%CONTENT%', $htmlContent, $articleTemplate);
        $articleTemplate = str_replace('%TAGS%', $tags, $articleTemplate);

        file_put_contents($targetPath . $articleMetadata->fileName . '.html', $articleTemplate);
    }

    public static function sanitize(string $line): string
    {
        $line = Normalizer::normalize($line);
        return htmlspecialchars($line, ENT_QUOTES | ENT_HTML5, 'UTF-8');
    }
}
