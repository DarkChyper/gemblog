<?php

namespace GemBlog\Tools;

class PublishPageTools
{
    public static function publishPages(array $articles): void {
        self::createPage('index', $articles);
        self::createPage('about');
        self::createPage('all_articles', $articles);
        self::createPage('articles_tags', $articles);
    }

    protected static function createPage(string $pageName, array $articles = []): void {
        $pageGmiContent = file_get_contents('./pages/' . $pageName . '.gmi');
        $pageJsonData = file_get_contents('./pages/' . $pageName);
        $pageData = json_decode($pageJsonData);

        if ('about' === $pageName) {
            $pageGmiContent = str_replace('%ATOM%', '=> ' . getenv('URL_TO_HTML') . 'atom_fr.xml', $pageGmiContent);
        }

        $pageGmiContent = str_replace('%TITLE%', $pageData->title, $pageGmiContent);
        $pageGmiContent = str_replace('%HTML_LINK%', getenv('URL_TO_HTML') . $pageData->fileName . '.html', $pageGmiContent);
        $pageGmiContent = str_replace('%GEM_LINK%', getenv('URL_TO_GMI') . $pageData->fileName . '.gmi', $pageGmiContent);

        file_put_contents(getenv('PATH_TO_PUBLISHED_GMI') . $pageData->fileName . '.gmi', $pageGmiContent);


        $pageGmiContent = file(getenv('PATH_TO_PUBLISHED_GMI') . $pageData->fileName . '.gmi');
        $htmlContent = '';
        $prevLine = '';
        foreach ($pageGmiContent as $line) {
            if (str_starts_with($line, '=> ./index.gmi Blog') || str_starts_with($line, '=> ./about.gmi A propos')) {
                continue;
            }
            $prevLine = GeminiTools::gmiToHtml($htmlContent, $line, $prevLine);
        }
        $htmlTemplate = file_get_contents('./templates/page.html');

        $htmlTemplate = str_replace('%LANG%', $pageData->lang, $htmlTemplate);
        $htmlTemplate = str_replace('%TITLE%', $pageData->title, $htmlTemplate);
        $htmlTemplate = str_replace('%HTML_LINK%', getenv('URL_TO_HTML') . $pageData->fileName . '.html', $htmlTemplate);
        $htmlTemplate = str_replace('%HTML_WEBSITE%', getenv('URL_TO_HTML'), $htmlTemplate);
        $htmlTemplate = str_replace('%GEM_LINK%', getenv('URL_TO_GMI') . $pageData->fileName . '.gmi', $htmlTemplate);
        $htmlTemplate = str_replace('%PUBLISHED_DATE%', $pageData->publishedAt, $htmlTemplate);
        $htmlTemplate = str_replace('%IMAGE_PREVIEW%', getenv('URL_TO_HTML') . 'assets/' . getenv('BLOG_BANNER'), $htmlTemplate);

        if ('index' === $pageName) {
            $htmlContent = self::processIndex($articles, $pageGmiContent, $htmlContent, $pageData);

        }

        if ('all_articles' === $pageName) {
            $prevYear = '';
            $articlesGmi = '';
            $articlesHtml = '';
            foreach ($articles as $article) {
                $year = substr($article['data']->publishedAt, 0, 4);
                if ($prevYear !== $year) {
                    if ($prevYear !== '') {
                        $articlesHtml .= '</ul>';
                    }

                    $prevYear = $year;
                    $articlesGmi .= '## ' . $year . chr(10);
                    $articlesHtml .= '<h3>' . $year . '</h3><ul>';
                }
                $articlesGmi .= '=> ./' . $article['data']->fileName . '.gmi '
                    . substr($article['data']->fileName, 0, 10) . ' : ' . $article['data']->title . chr(10);
                $articlesHtml .= '<li><a href="./' . $article['data']->fileName . '.html">'
                    . substr($article['data']->fileName, 0, 10) . ' : ' . $article['data']->title . '</a></li>';
            }
            $articlesHtml .= '</ul>';
            $pageGmiContent = str_replace('%ARTICLES%', $articlesGmi, $pageGmiContent);
            $pageGmiContent = str_replace('%SUBTITLE%', getenv('BLOG_SUBTITLE_FR'), $pageGmiContent);

            $htmlContent = str_replace('%ARTICLES%', $articlesHtml, $htmlContent);
            $htmlContent = str_replace('%SUBTITLE%', '<p class="subtitle">' . getenv('BLOG_SUBTITLE_FR') . '</p>', $htmlContent);

            file_put_contents(getenv('PATH_TO_PUBLISHED_GMI') . $pageData->fileName . '.gmi', $pageGmiContent);
        }

        if ('articles_tags' === $pageName) {
            $htmlContent = self::processTags($articles, $pageGmiContent, $htmlContent, $pageData);
        }

        $htmlTemplate = str_replace('%CONTENT%', $htmlContent, $htmlTemplate);
        file_put_contents(getenv('PATH_TO_PUBLISHED_HTML') . $pageData->fileName . '.html', $htmlTemplate);
    }

    public static function processIndex(
        array  $published,
        array  $pageGmiContent,
        string $htmlContent,
        mixed  $pageData): string
    {
        $out = 0;

        $articlesGmi = '';
        $articlesHtml = '<ul>';
        foreach ($published as $article) {
            if (5 <= $out) {
                break;
            }
            $articlesGmi .= '=> ./' . $article['data']->fileName . '.gmi '
                . substr($article['data']->fileName, 0, 10) . ' : ' . $article['data']->title . chr(10);

            $articlesHtml .= '<li><a href="./' . $article['data']->fileName . '.html">'
                . substr($article['data']->fileName, 0, 10) . ' : ' . $article['data']->title . '</a></li>';
            $out++;
        }
        $articlesHtml .= '</ul>';

        $pageGmiContent = str_replace('%ARTICLES%', $articlesGmi, $pageGmiContent);
        $pageGmiContent = str_replace('%SUBTITLE%', getenv('BLOG_SUBTITLE_FR'), $pageGmiContent);

        $htmlContent = str_replace('%ARTICLES%', $articlesHtml, $htmlContent);
        $htmlContent = str_replace('%SUBTITLE%', '<p class="subtitle">' . getenv('BLOG_SUBTITLE_FR') . '</p>', $htmlContent);

        file_put_contents(getenv('PATH_TO_PUBLISHED_GMI') . $pageData->fileName . '.gmi', $pageGmiContent);
        return $htmlContent;
    }

    protected static function processTags(
        array  $published,
        array  $pageGmiContent,
        string $htmlContent,
        mixed  $pageData): string
    {
        $articlesByTags = [];
        foreach ($published as $article) {
            foreach ($article['data']->tags as $tag) {
                if (!isset($articlesByTags[$tag])) {
                    $articlesByTags[$tag] = [];
                }
                $articlesByTags[$tag][] = $article['data']->fileName;
            }
        }
        uksort($articlesByTags, function ($a, $b) {
            return strnatcmp($a, $b);
        });
        $tagsGmi = '';
        $tagsHtml = '<ul>';
        foreach (array_keys($articlesByTags) as $tag) {
            $tagsGmi .= '=> ./articles_' . $tag . '.gmi #' . $tag . chr(10);

            $tagsHtml .= '<li><a href="./articles_' . $tag . '.html">#' . $tag . '</a></li>';
        }
        $tagsHtml .= '</ul>';
        $pageGmiContent = str_replace('%TAGS%', $tagsGmi, $pageGmiContent);
        $pageGmiContent = str_replace('%SUBTITLE%', getenv('BLOG_SUBTITLE_FR'), $pageGmiContent);

        $htmlContent = str_replace('%TAGS%', $tagsHtml, $htmlContent);
        $htmlContent = str_replace('%SUBTITLE%', '<p class="subtitle">' . getenv('BLOG_SUBTITLE_FR') . '</p>', $htmlContent);

        file_put_contents(getenv('PATH_TO_PUBLISHED_GMI') . $pageData->fileName . '.gmi', $pageGmiContent);

        foreach ($articlesByTags as $tag => $articles) {
            $htmlTemplate = file_get_contents('./templates/page.html');
            $gmiTemplate = file_get_contents('./templates/page.gmi');
            $articlesGmi = '';
            $articlesHtml = '<ul>';

            foreach ($articles as $article) {
                $articlesGmi .= '=> ./' . $article . '.gmi ' . $article;
                $articlesHtml .= '<li><a href="./' . $article . '.html">' . $article . '</a></li>';
            }
            $articlesHtml .= '</ul>';

            $gmiTemplate = str_replace('%CONTENT%', $articlesGmi, $gmiTemplate);
            $gmiTemplate = str_replace('%TITLE%', 'Articles contenant le tag #' . $tag, $gmiTemplate);

            $htmlTemplate = str_replace('%CONTENT%', $articlesHtml, $htmlTemplate);
            $htmlTemplate = str_replace('%TITLE%', 'Articles contenant le tag #' . $tag, $htmlTemplate);
            $htmlTemplate = str_replace('%LANG%', 'fr', $htmlTemplate);
            $htmlTemplate = str_replace('%HTML_LINK%', getenv('URL_TO_HTML') . 'articles_' . $tag . '.html', $htmlTemplate);
            $htmlTemplate = str_replace('%HTML_WEBSITE%', getenv('URL_TO_HTML'), $htmlTemplate);
            $htmlTemplate = str_replace('%PUBLISHED_DATE%', (new \DateTime())->format('Y:m:d\TH:i:s\Z'), $htmlTemplate);
            $htmlTemplate = str_replace('%IMAGE_PREVIEW%', getenv('URL_TO_HTML') . 'assets/' . getenv('BLOG_BANNER'), $htmlTemplate);

            file_put_contents(getenv('PATH_TO_PUBLISHED_GMI') . 'articles_' . $tag . '.gmi', $gmiTemplate);
            file_put_contents(getenv('PATH_TO_PUBLISHED_HTML') . 'articles_' . $tag . '.html', $htmlTemplate);
        }

        return $htmlContent;

    }
}
