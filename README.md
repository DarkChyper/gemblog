# GemBlog

A "simple" PHP script to generate Gemini capsules and HTML Pages as a blog.

It's difficult to make things simple, so i make it more complex as it should be.

You can write text in gemtext (.gmi) or in my format gemini X (.gmix).

Texts and script are stored in the same repository so you only need to pull on your server and execute the script php publish.php to publish new posts.

## Installation

Get the source of this project locally, where you want to publish your blog.

Install PHP 8.1 or higher and the following dependencies:

```sudo apt install php8.1-intl php8.1-gd libapache2-mod-php8.1 php8.1-curl```

Duplicate the .env file to .env.local and fill in your information there.
The "LANGUAGES" entry is a simple string of characters with 2 letters per language and separated by a comma.

Each link, url or folder path must end with a slash '/'.

Create the language folders at the root of the project, and the gemini and html folders to publish the articles. (For example the public_html and public_gemini folders in the home folder)
In the folders to publish, create a folder named "assets".

Make sure that all folders are accessible and writable for php user and www-data.



## Environment Variables

To run this project, you will need to add the following environment variables to your .env.local file

`LANGUAGES`  
`MAIN_LANGUAGE`  
`PATH_TO_SOURCE`  
`PATH_TO_PUBLISHED_GMI`  
`PATH_TO_PUBLISHED_HTML`  
`URL_TO_GMI`  
`URL_TO_HTML`  
`BLOG_AUTHOR`  
`BLOG_NAME`  
`BLOG_SUBTITLE_FR`  
`BLOG_SUBTITLE_EN`  
`BLOG_BANNER` 
`IMAGE_MAX_WIDTH`
`CODE_LANGUAGE='/^ASMB|BASH|BASIC|C|CPP|CSHARP|COBOL|CPP|CSS|GMI|GMIX|JAVA|JS|KSH|PERL|PHP|PYTHON|R|RUST|SPOILER|TS|YAML/'` 
`MASTODON_URL`  
`MASTODON_ACCESS_TOKEN`  


## Usage/Examples

### Pages and templates

In addition to blog posts, there are a few pages that are hard-coded in the ./pages folder.  

You must adapt this content to your own blog and in your language. 

These pages are not translated or duplicated for each language you have defined.

You must also modify the files without the .gmi of this same file (pages only, not templates) to put the correct information there:

```
{
    "hash": "",
    "lang": "",
    "title": "",
    "author": "",
    "description": "",
    "tags": [],
    "banner": {
        "filePath": "",
        "fileDescription": ""
    },
    "fileName": "about",
    "publishedAt": "2023-04-14T00:00:00",
    "updatedAt": "2023-04-14T00:00:00",
    "urlToot": ""
}
```

You don't need to fill the hash, tags, banner and urlToot but all other parameters must be defined.

Thes pages are republished any time.

### Writing a blog post 

Each blog post is store in his language folder. It' recommended to use only alphanumeric caracters and '-' in the name because it will be his url.

Each blog post must be written in gemtext [https://gemini.circumlunar.space/docs/cheatsheet.gmi](https://gemini.circumlunar.space/docs/cheatsheet.gmi) and could use the overlay gemini X. If you use .gmix, file must start with these 5 lines : 

```
!# My blog post title, not empty
!# A brief description of the post, using to toot on mastodon (like tags)
!# A list of tags, separated by comma
!# banner.jpg A path to the banner of the post with a description, separated by space, could be empty
!# alternate author of the AUTHOR in .env.local file, replace by the default author if empty
``` 

To be considered publishable, a blog post must have its filename begin with a date in the format "YYYY-MM-DD". Otherwise, it will be ignored by the publish.php script.

If the date is in the future, the article is ignore until the date is arrived.

### Publish

To publish, use the php script publish.php in a command line.
This script need 2 boolean parameters to works.

```php ./publish.php true true```

The first one is "newPublishedOnly", if true, only unpublished articles will be considered. 

The second one is "sendToot", if false publish old or new articles will not trigger the toot sending.

Each published post is toot with 2 messages, the first one with description, url to the HTML format ans tags. The second is a reply of the first one, with the link to the gemini format.
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are greatly appreciated.

Check that your question or new idea does not already exist in the issues. If not, create a new request explaining clearly.

If you have the skills and the desire to participate, do not hesitate to develop your patch or your tool and propose it in PR


## Authors

- [@DarkChyper](https://gitlab.com/DarkChyper)


## License

Distributed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International. See [LICENSE](LICENSE) for more information.

